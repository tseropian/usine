<?php
define('DRUPAL_ROOT', '/home/usineio/public_devusineio');
include_once(DRUPAL_ROOT . '/includes/bootstrap.inc');
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);


require '/home/usineio/vendor/paymill/paymill/autoload.php';
$request = new Paymill\Request('993e7d890568bd9f83caf6dacf5bacad');



$query = new EntityFieldQuery();
$query->entityCondition('entity_type', 'commerce_product')->entityCondition('bundle', 'product')->propertyCondition('status', 1)->fieldOrderBy('commerce_price', 'amount', 'ASC');
$result = $query->execute();

foreach ($result['commerce_product'] as $key => $val) {

        $product = commerce_product_load($val->product_id);
	$product->field_usine_paymill_offer_id['und'][0]['value'] = "1234";
	$product_price_ht = $product->commerce_price['und'][0]['amount'];
	$product_price_ttc = $product_price_ht * 1.20;
	echo $product_price_ttc . "#";

	$offer = new Paymill\Models\Request\Offer();
	$offer->setAmount($product_price_ttc)->setCurrency('EUR')->setInterval('1 MONTH')->setName($product->sku);

	$response = $request->create($offer);
        $product->field_usine_paymill_offer_id['und'][0]['value'] = $response->getId();

	commerce_product_save($product); 	
}

/*
$offer = new Paymill\Models\Request\Offer();
$offer->setAmount(4200)->setCurrency('EUR')->setInterval('1 MONTH')->setName('Test Offer');

$response = $request->create($offer);
$toto = $response->getId();;

var_dump($toto);
*/

?>
