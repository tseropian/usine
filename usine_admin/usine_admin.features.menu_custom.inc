<?php
/**
 * @file
 * usine_admin.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function usine_admin_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-usine-io.
  $menus['menu-usine-io'] = array(
    'menu_name' => 'menu-usine-io',
    'title' => 'Usine.io',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => 0,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Usine.io');


  return $menus;
}
