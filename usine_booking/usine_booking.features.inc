<?php
/**
 * @file
 * usine_booking.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function usine_booking_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function usine_booking_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function usine_booking_node_info() {
  $items = array(
    'booking' => array(
      'name' => t('Booking'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'machine' => array(
      'name' => t('Machine'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'room' => array(
      'name' => t('Room'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Room'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
