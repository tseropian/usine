<?php
/**
 * @file
 * usine_booking.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function usine_booking_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_booking_room|node|booking|form';
  $field_group->group_name = 'group_booking_room';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'booking';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Room Booking',
    'weight' => '7',
    'children' => array(
      0 => 'field_booking_order',
      1 => 'field_room_booking_interval',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-booking-room field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_booking_room|node|booking|form'] = $field_group;

  return $export;
}
