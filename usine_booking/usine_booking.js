(function ($) {
$(document).ready(function(){

	$('.form-item-field-booking-start-date-und-0-value2').css("display", "none");

        var start_date  = $('#edit-field-booking-start-date-und-0-value-datepicker-popup-0').val();
        var start_time  = $('#edit-field-booking-start-date-und-0-value-timeEntry-popup-1').val();

        var obj_date    = start_date.split("-");
        var obj_time    = start_time.split(":");

        var data        = $('#edit-field-machine-booking-interval-und').val();

        var d1          = new Date(obj_date[0], obj_date[1]-1, obj_date[2], obj_time[0], obj_time[1]);
        var d2          = new Date(d1.getTime() + data*60000);

        var hour        = d2.getHours();
        var minute      = d2.getMinutes();
        $('#edit-field-booking-start-date-und-0-value2-timeEntry-popup-1').val(hour + ':' + minute);


$('#edit-field-booking-start-date-und-0-value-timeEntry-popup-1').change(function(){

	var start_date  = $('#edit-field-booking-start-date-und-0-value-datepicker-popup-0').val();
        var start_time  = $(this).val();
	
        var obj_date    = start_date.split("-");
        var obj_time    = start_time.split(":");

	var data 	= $('#edit-field-machine-booking-interval-und').val();

        var d1          = new Date(obj_date[0], obj_date[1]-1, obj_date[2], obj_time[0], obj_time[1]);
        var d2          = new Date(d1.getTime() + data*60000);

        var hour        = d2.getHours();
        var minute      = d2.getMinutes();

        $('#edit-field-booking-start-date-und-0-value2-timeEntry-popup-1').val(hour + ':' + minute);

});

$('#edit-field-machine-booking-interval-und').change(function(){

	var data        = $(this).val();
        var start_date  = $('#edit-field-booking-start-date-und-0-value-datepicker-popup-0').val();
        var start_time  = $('#edit-field-booking-start-date-und-0-value-timeEntry-popup-1').val();

        var obj_date    = start_date.split("-");
        var obj_time    = start_time.split(":");

        var d1          = new Date(obj_date[0], obj_date[1]-1, obj_date[2], obj_time[0], obj_time[1]);
        var d2 		= new Date(d1.getTime() + data*60000);

	var year 	= d2.getFullYear();
	var month 	= d2.getMonth()+1;
	var day 	= d2.getDate();
	var hour 	= d2.getHours();
	var minute 	= d2.getMinutes();

	$('#edit-field-booking-start-date-und-0-value2-datepicker-popup-0').val(year + '-' + month + '-' + day);
	$('#edit-field-booking-start-date-und-0-value2-timeEntry-popup-1').val(hour + ':' + minute);
 
   });

});

})(jQuery);


(function ($, Drupal, window, document, undefined) {jQuery(document).ready(function(){
      // When the 'from date' is changed on the following date fields, set
      // the 'to date' equal to the 'from date'.
      var date_fields_default_to_from = new Array(
	'field_booking_start_date'
      );
      var css_selector = '';
      $.each(date_fields_default_to_from, function(index, field){
          css_selector += '#edit-' + field.replace(/_/g,'-') + '-und-0-value-datepicker-popup-0, ';
      });
      css_selector = css_selector.substring(0, css_selector.length - 2); // Remove last ', '
      $(css_selector).change(function(){
          $('#' + $(this).attr('id').replace(/value/g, 'value2')).val($(this).val());
      });
});})(jQuery, Drupal, this, this.document);
