<?php
/**
 * @file
 * usine_core.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function usine_core_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Usine.io address';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'usine_address';
  $fe_block_boxes->body = '<p><span class="bold">USINE IO</span><br>181 - 183 rue du Chevaleret<br>75013 Paris</p>';

  $export['usine_address'] = $fe_block_boxes;

  return $export;
}
