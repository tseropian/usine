<?php
/**
 * @file
 * usine_core.features.inc
 */

/**
 * Implements hook_default_cl_billing_cycle_type().
 */
function usine_core_default_cl_billing_cycle_type() {
  $items = array();
  $items['usine_billing_monthly'] = entity_import('cl_billing_cycle_type', '{
    "engine" : "periodic",
    "name" : "usine_billing_monthly",
    "title" : "Usine Billing Monthly",
    "prepaid" : null,
    "wrapper" : {},
    "pce_period" : { "und" : [ { "value" : "month" } ] },
    "pce_async" : { "und" : [ { "value" : "1" } ] },
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function usine_core_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function usine_core_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function usine_core_node_info() {
  $items = array(
    'badge' => array(
      'name' => t('Badge'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Badge ID'),
      'help' => '',
    ),
    'event' => array(
      'name' => t('Event'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'startup' => array(
      'name' => t('Entreprise'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Startup Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
