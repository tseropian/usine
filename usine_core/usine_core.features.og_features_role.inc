<?php
/**
 * @file
 * usine_core.features.og_features_role.inc
 */

/**
 * Implements hook_og_features_default_roles().
 */
function usine_core_og_features_default_roles() {
  $roles = array();

  // Exported OG Role: 'node:startup:staff member'.
  $roles['node:startup:staff member'] = array(
    'gid' => 0,
    'group_type' => 'node',
    'group_bundle' => 'startup',
    'name' => 'staff member',
  );

  return $roles;
}
