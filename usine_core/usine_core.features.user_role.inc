<?php
/**
 * @file
 * usine_core.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function usine_core_user_default_roles() {
  $roles = array();

  // Exported role: active member.
  $roles['active member'] = array(
    'name' => 'active member',
    'weight' => 7,
  );

  // Exported role: corporate.
  $roles['corporate'] = array(
    'name' => 'corporate',
    'weight' => 11,
  );

  // Exported role: incubator member.
  $roles['incubator member'] = array(
    'name' => 'incubator member',
    'weight' => 9,
  );

  // Exported role: individual.
  $roles['individual'] = array(
    'name' => 'individual',
    'weight' => 3,
  );

  // Exported role: startup member.
  $roles['startup member'] = array(
    'name' => 'startup member',
    'weight' => 4,
  );

  // Exported role: student.
  $roles['student'] = array(
    'name' => 'student',
    'weight' => 8,
  );

  // Exported role: usine core.
  $roles['usine core'] = array(
    'name' => 'usine core',
    'weight' => 6,
  );

  // Exported role: usine staff.
  $roles['usine staff'] = array(
    'name' => 'usine staff',
    'weight' => 10,
  );

  // Exported role: vip.
  $roles['vip'] = array(
    'name' => 'vip',
    'weight' => 5,
  );

  return $roles;
}
