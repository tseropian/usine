<?php
/**
 * @file
 * usine_core.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function usine_core_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_event_details|node|event|form';
  $field_group->group_name = 'group_event_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'event';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Event Details',
    'weight' => '5',
    'children' => array(
      0 => 'field_event_registration',
      1 => 'field_event_date',
      2 => 'field_event_technical_level',
      3 => 'field_event_machine_type',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-event-details field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_event_details|node|event|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_startup_usine_io|node|startup|form';
  $field_group->group_name = 'group_startup_usine_io';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'startup';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Usine.io Membership',
    'weight' => '5',
    'children' => array(
      0 => 'field_startup_active',
      1 => 'field_startup_coworking_space',
      2 => 'field_startup_usine_tags',
      3 => 'field_startup_vip',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-startup-usine-io field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_startup_usine_io|node|startup|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_user_usine_io|user|user|form';
  $field_group->group_name = 'group_user_usine_io';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Usine.io Membership',
    'weight' => '7',
    'children' => array(
      0 => 'field_user_vip',
      1 => 'field_user_usine_comments',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-user-usine-io field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_user_usine_io|user|user|form'] = $field_group;

  return $export;
}
