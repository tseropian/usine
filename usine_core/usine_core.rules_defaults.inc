<?php
/**
 * @file
 * usine_core.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function usine_core_default_rules_configuration() {
  $items = array();
  $items['rules_usine_core_logout_redirect'] = entity_import('rules_config', '{ "rules_usine_core_logout_redirect" : {
      "LABEL" : "Usine - Core - Logout - Redirect",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "user_logout" : [] },
      "DO" : [ { "redirect" : { "url" : "http:\\/\\/usine.io" } } ]
    }
  }');
  $items['rules_usine_redirect_to_cart'] = entity_import('rules_config', '{ "rules_usine_redirect_to_cart" : {
      "LABEL" : "Usine - Redirect to Cart",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "usine.io" ],
      "REQUIRES" : [ "rules", "commerce_cart" ],
      "ON" : { "commerce_cart_product_add" : [] },
      "DO" : [ { "redirect" : { "url" : "cart" } } ]
    }
  }');
  return $items;
}
