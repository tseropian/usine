<?php

/**
 * Variable definition
 */

/**
 * Implements hook_variable_group_info()
 */
function usine_email_variable_group_info() {
  // Group for variable that have no group
  $groups['usine_email'] = array(
    'title' => t('Usine Email'),
    'description' => t("Emails used in Usine;io CRM"),
  );
  return $groups;
}

/**
 * Implements hook_variable_info().
 */
function usine_email_variable_info($options) {

  $variable['usine_email_expire_notice_[mail_part]'] = array(
    'title' => t('Usine Email - Expiration Notification to users'),
    'type' => 'mail_html',
    'description' => t('Template for the order email sent out to the customer.'),
    'children' => array(
      'usine_email_expire_notice_subject' => array(
        'default' => 'Order [commerce-order:order-number] at [site:name] !!!',
      ),
      'usine_email_expire_notice_body' => array(
        'default' => '<p>Thanks for your order [commerce-order:order-number] at [site:name].</p><p>[commerce-order:commerce-email-order-items]</p><p>If this is your first order with us, you will receive a separate e-mail with login instructions.</p><p>You can view your order history with us at any time by logging into our website at: <a href="[site:login-url]">[site:login-url]</a></p><p>You can view the status of your current order at: <a href="[site:url]user/[commerce-order:uid]/orders/[commerce-order:order-id]">[site:url]user/[commerce-order:uid]/orders/[commerce-order:order-id]</a></p><p>Please contact us if you have any questions about your order.</p>'
      ),
    ),
    'group' => 'usine_email'
  );

  $variable['usine_email_termination_confirmation_[mail_part]'] = array(
    'title' => t('Usine Email - Confirmation of Membership Deleting'),
    'type' => 'mail_html',
    'description' => t('Template for the order email sent out when user membership ends '),
    'children' => array(
      'usine_email_termination_confirmation_subject' => array(
        'default' => '',
      ),
      'usine_email_termination_confirmation_body' => array(
        'default' => '<p>Thanks for your order [commerce-order:order-number] at [site:name].</p><p>[commerce-order:commerce-email-order-items]</p><p>If this is your first order with us, you will re',
      ),
    ),
    'group' => 'usine_email'
  );


  $variable['usine_email_expiration_confirmation_[mail_part]'] = array(
    'title' => t('Usine Email - Confirmation of Membership Cancelling'),
    'type' => 'mail_html',
    'description' => t('Template for the email sent out when user cancels his membership'),
    'children' => array(
      'usine_email_expiration_confirmation_subject' => array(
        'default' => 'Order [commerce-order:order-number] at [site:name]',
      ),
      'usine_email_expiration_confirmation_body' => array(
        'default' => '<p>Thanks for your order [commerce-order:order-number] at [site:name].</p><p>[commerce-order:commerce-email-order-items]</p><p>If this is your first order with us, you will re',
      ),
    ),
    'group' => 'usine_email'
  );


  $variable['usine_email_expiration_confirmation_[mail_part]'] = array(
    'title' => t('Usine Email - Confirmation of Membership Cancelling'),
    'type' => 'mail_html',
    'description' => t('Template for the email sent out when user cancels his membership'),
    'children' => array(
      'usine_email_expiration_confirmation_subject' => array(
        'default' => 'Order [commerce-order:order-number] at [site:name]',
      ),
      'usine_email_expiration_confirmation_body' => array(
        'default' => '',
      ),
    ),
    'group' => 'usine_email'
  );


  $variable['usine_email_payment_failure_user_[mail_part]'] = array(
    'title' => t('Usine Email - Payment Failure Notice - User'),
    'type' => 'mail_html',
    'description' => t('Template for the email sent out when a payment failure happens'),
    'children' => array(
      'usine_email_payment_failure_user_subject' => array(
        'default' => 'Erreur de paiement',
      ),
      'usine_email_payment_failure_user_body' => array(
        'default' => '<p>Bonjour,</p><p>Votre transaction a rencontré un problème et n&#39;a pas été prise en compte. Aucun débit n&#39;a été effectué sur votre compte.</p><p>L&#39;équipe d&#39;USINE IO vous contacte dans les plus brefs délais.</p><p>Merci</p><p>Remarque : ce courriel a été généré automatiquement par Usine IO.</p>',
      ),
    ),
    'group' => 'usine_email'
  );


  $variable['usine_email_monthly_payment_notification_user_[mail_part]'] = array(
    'title' => t('Usine Email - Payment Notice - User'),
    'type' => 'mail_html',
    'description' => t('Template for the email sent out when a monthly payment is done.'),
    'children' => array(
      'usine_email_monthly_payment_notification_user_subject' => array(
        'default' => 'Votre paiement mensuel Usine IO',
      ),
      'usine_email_monthly_payment_notification_user_body' => array(
        'default' => '<p>Bonjour,</p><p>Votre abonnement mensuel Usine IO va être prélevé dans les prochains jours. Vous pourrez trouver la facture correspondante dans votre espace client.</p><p>Merci</p><p>Remarque : ce courriel a été généré automatiquement par Usine IO.</p>',
      ),
    ),
    'group' => 'usine_email'
  );

  $variable['usine_email_monthly_payment_error_user_[mail_part]'] = array(
    'title' => t('Usine Email - Monthly Payment - Error - User'),
    'type' => 'mail_html',
    'description' => t('Template for the email sent out when a monthly payment is failed'),
    'children' => array(
      'usine_email_monthly_payment_error_user_subject' => array(
        'default' => 'Votre paiement mensuel Usine IO',
      ),
      'usine_email_monthly_payment_error_user_body' => array(
        'default' => '<p>Bonjour,</p><p>Votre prélèvement de ce mois a rencontré un problème. Merci de régulariser votre situation et de nous tenir informé au plus vite.</p><p>Sans retour de votre part, une nouvelle tentative sera faite dans les prochains jours. Si toutefois cette seconde tentative n’était pas concluante, nous serons au regret de devoir suspendre votre accès à Usine IO.</p><p>Nous nous tenons disponibles pour répondre à vos questions.</p><p>Merci</p><p>L’équipe Usine IO</p>'
      ),
    ),
    'group' => 'usine_email'
  );


  $variable['usine_email_expiration_card_user_[mail_part]'] = array(
    'title' => t('Usine Email - Payment Card Expire'),
    'type' => 'mail_html',
    'description' => t('Template payment card expiring - Notification'),
    'children' => array(
      'usine_email_expiration_card_user_subject' => array(
        'default' => 'Votre paiement mensuel Usine IO',
      ),
      'usine_email_expiration_card_user_body' => array(
        'default' => '<p>Bonjour,</p><p>Votre prélèvement de ce mois a rencontré un problème. Merci de régulariser votre situation et de nous tenir informé au plus vite.</p><p>Sans retour de votre part, un',
      ),
    ),
    'group' => 'usine_email'
  );



 return $variable;
}


