<?php
/**
 * @file
 * usine_imports.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function usine_imports_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'usine_products_imports';
  $feeds_importer->config = array(
    'name' => 'Usine Products Imports',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
        'directory' => 'private://feeds',
        'allowed_schemes' => array(
          0 => 'public',
          1 => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsCommerceProductMultiProcessor',
      'config' => array(
        'product_type' => 'product',
        'author' => '1',
        'tax_rate' => TRUE,
        'mappings' => array(
          0 => array(
            'source' => 'Référence',
            'target' => 'sku',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'Titre',
            'target' => 'title_field',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'License billing cycle type',
            'target' => 'cl_billing_cycle_type:label',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'License billing type',
            'target' => 'cl_billing_type',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'License Nb Staff',
            'target' => 'field_license_nb_staff',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Membership Length',
            'target' => 'field_usine_membership_duration',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'Prix',
            'target' => 'commerce_price:amount',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'Rôle',
            'target' => 'commerce_license_role',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'Type de licence',
            'target' => 'commerce_license_type',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'product',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => 1800,
    'expire_period' => 3600,
    'import_on_create' => TRUE,
    'process_in_background' => FALSE,
  );
  $export['usine_products_imports'] = $feeds_importer;

  return $export;
}
