<?php
/**
 * @file
 * usine_imports.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function usine_imports_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'usine_admin_exports_products';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'commerce_product';
  $view->human_name = 'Usine - Admin - Products - Exports ';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Usine - Admin - Products - Exports ';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'plus';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Appliquer';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Réinitialiser';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Trier par';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title_field' => 'title_field',
    'language' => 'language',
    'cl_billing_cycle_type' => 'cl_billing_cycle_type',
    'cl_billing_type' => 'cl_billing_type',
    'commerce_license_duration' => 'commerce_license_duration',
    'field_license_nb_staff' => 'field_license_nb_staff',
    'field_usine_membership_duration' => 'field_usine_membership_duration',
    'cl_schedule_changes' => 'cl_schedule_changes',
    'delta' => 'delta',
    'commerce_price' => 'commerce_price',
    'commerce_license_role' => 'commerce_license_role',
    'commerce_license_type' => 'commerce_license_type',
    'sku' => 'sku',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title_field' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'language' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'cl_billing_cycle_type' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'cl_billing_type' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'commerce_license_duration' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_license_nb_staff' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_usine_membership_duration' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'cl_schedule_changes' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'delta' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'commerce_price' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'commerce_license_role' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'commerce_license_type' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'sku' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Champ: Commerce Product : Référence */
  $handler->display->display_options['fields']['sku']['id'] = 'sku';
  $handler->display->display_options['fields']['sku']['table'] = 'commerce_product';
  $handler->display->display_options['fields']['sku']['field'] = 'sku';
  $handler->display->display_options['fields']['sku']['link_to_product'] = 0;
  /* Champ: Champ : Titre */
  $handler->display->display_options['fields']['title_field']['id'] = 'title_field';
  $handler->display->display_options['fields']['title_field']['table'] = 'field_data_title_field';
  $handler->display->display_options['fields']['title_field']['field'] = 'title_field';
  $handler->display->display_options['fields']['title_field']['link_to_entity'] = 0;
  /* Champ: Commerce Product : Langue */
  $handler->display->display_options['fields']['language']['id'] = 'language';
  $handler->display->display_options['fields']['language']['table'] = 'commerce_product';
  $handler->display->display_options['fields']['language']['field'] = 'language';
  /* Champ: Commerce Product : License billing cycle type */
  $handler->display->display_options['fields']['cl_billing_cycle_type']['id'] = 'cl_billing_cycle_type';
  $handler->display->display_options['fields']['cl_billing_cycle_type']['table'] = 'field_data_cl_billing_cycle_type';
  $handler->display->display_options['fields']['cl_billing_cycle_type']['field'] = 'cl_billing_cycle_type';
  $handler->display->display_options['fields']['cl_billing_cycle_type']['settings'] = array(
    'link' => 0,
  );
  /* Champ: Commerce Product : License billing type */
  $handler->display->display_options['fields']['cl_billing_type']['id'] = 'cl_billing_type';
  $handler->display->display_options['fields']['cl_billing_type']['table'] = 'field_data_cl_billing_type';
  $handler->display->display_options['fields']['cl_billing_type']['field'] = 'cl_billing_type';
  /* Champ: Commerce Product : License duration */
  $handler->display->display_options['fields']['commerce_license_duration']['id'] = 'commerce_license_duration';
  $handler->display->display_options['fields']['commerce_license_duration']['table'] = 'field_data_commerce_license_duration';
  $handler->display->display_options['fields']['commerce_license_duration']['field'] = 'commerce_license_duration';
  $handler->display->display_options['fields']['commerce_license_duration']['settings'] = array(
    'thousand_separator' => ' ',
    'prefix_suffix' => 1,
  );
  /* Champ: Commerce Product : License Nb Staff */
  $handler->display->display_options['fields']['field_license_nb_staff']['id'] = 'field_license_nb_staff';
  $handler->display->display_options['fields']['field_license_nb_staff']['table'] = 'field_data_field_license_nb_staff';
  $handler->display->display_options['fields']['field_license_nb_staff']['field'] = 'field_license_nb_staff';
  $handler->display->display_options['fields']['field_license_nb_staff']['settings'] = array(
    'thousand_separator' => ' ',
    'prefix_suffix' => 1,
  );
  /* Champ: Commerce Product : Membership Length */
  $handler->display->display_options['fields']['field_usine_membership_duration']['id'] = 'field_usine_membership_duration';
  $handler->display->display_options['fields']['field_usine_membership_duration']['table'] = 'field_data_field_usine_membership_duration';
  $handler->display->display_options['fields']['field_usine_membership_duration']['field'] = 'field_usine_membership_duration';
  $handler->display->display_options['fields']['field_usine_membership_duration']['settings'] = array(
    'thousand_separator' => ' ',
    'prefix_suffix' => 1,
  );
  /* Champ: Commerce Product : Postpone license changes */
  $handler->display->display_options['fields']['cl_schedule_changes']['id'] = 'cl_schedule_changes';
  $handler->display->display_options['fields']['cl_schedule_changes']['table'] = 'field_data_cl_schedule_changes';
  $handler->display->display_options['fields']['cl_schedule_changes']['field'] = 'cl_schedule_changes';
  $handler->display->display_options['fields']['cl_schedule_changes']['delta_offset'] = '0';
  /* Champ: Commerce Product : Postpone license changes (cl_schedule_changes:delta) */
  $handler->display->display_options['fields']['delta']['id'] = 'delta';
  $handler->display->display_options['fields']['delta']['table'] = 'field_data_cl_schedule_changes';
  $handler->display->display_options['fields']['delta']['field'] = 'delta';
  /* Champ: Commerce Product : Price */
  $handler->display->display_options['fields']['commerce_price']['id'] = 'commerce_price';
  $handler->display->display_options['fields']['commerce_price']['table'] = 'field_data_commerce_price';
  $handler->display->display_options['fields']['commerce_price']['field'] = 'commerce_price';
  $handler->display->display_options['fields']['commerce_price']['label'] = 'Prix';
  $handler->display->display_options['fields']['commerce_price']['click_sort_column'] = 'amount';
  $handler->display->display_options['fields']['commerce_price']['type'] = 'commerce_price_raw_amount';
  $handler->display->display_options['fields']['commerce_price']['settings'] = array(
    'calculation' => '0',
  );
  /* Champ: Commerce Product : Rôle */
  $handler->display->display_options['fields']['commerce_license_role']['id'] = 'commerce_license_role';
  $handler->display->display_options['fields']['commerce_license_role']['table'] = 'field_data_commerce_license_role';
  $handler->display->display_options['fields']['commerce_license_role']['field'] = 'commerce_license_role';
  /* Champ: Commerce Product : Type de licence */
  $handler->display->display_options['fields']['commerce_license_type']['id'] = 'commerce_license_type';
  $handler->display->display_options['fields']['commerce_license_type']['table'] = 'field_data_commerce_license_type';
  $handler->display->display_options['fields']['commerce_license_type']['field'] = 'commerce_license_type';
  /* Critère de filtrage: Commerce Product : Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'commerce_product';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'product' => 'product',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/usine/products/export';

  /* Display: Exportation de données */
  $handler = $view->new_display('views_data_export', 'Exportation de données', 'views_data_export_1');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['path'] = 'admin/usine/products/exports';
  $handler->display->display_options['displays'] = array(
    'page' => 'page',
    'default' => 0,
  );
  $translatables['usine_admin_exports_products'] = array(
    t('Master'),
    t('Usine - Admin - Products - Exports '),
    t('plus'),
    t('Appliquer'),
    t('Réinitialiser'),
    t('Trier par'),
    t('Asc'),
    t('Desc'),
    t('Référence'),
    t('Titre'),
    t('Langue'),
    t('License billing cycle type'),
    t('License billing type'),
    t('License duration'),
    t('License Nb Staff'),
    t('Membership Length'),
    t('Postpone license changes'),
    t('Postpone license changes (cl_schedule_changes:delta)'),
    t('.'),
    t(','),
    t('Prix'),
    t('Rôle'),
    t('Type de licence'),
    t('Page'),
    t('Exportation de données'),
  );
  $export['usine_admin_exports_products'] = $view;

  return $export;
}
