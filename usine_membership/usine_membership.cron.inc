<?php

function usine_membership_cron() {

  watchdog("Usine Membership", "Starting Membership Cron");

  $query  = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'commerce_license');
  $result = $query->execute();

  foreach ($result['commerce_license'] as $key => $val) {
    $license                = entity_load_single('commerce_license', $val->license_id);
    $license_user           = user_load($license->uid);

    if($license->status == 2) {

      $license_product        = commerce_product_load($license->product_id);
      $timestamp_expire       = strtotime(date('Y-m-d', $license->granted) . ' +' . $license_product->field_usine_membership_duration['und'][0]['value'] . ' months');
      $timestamp_now          = time();


      /*
       * Listing for current user last orders and generating monthly order
       * @todo : to be generated through Paymill webhook
       */
      $query_order = new EntityFieldQuery();
      $query_order->entityCondition('entity_type', 'commerce_order')->propertyCondition('uid', $license->uid)->propertyCondition('type', 'commerce_order')->propertyOrderBy('created', 'DESC')->range(0,1);
      $result_order = $query_order->execute();

      foreach ($result_order['commerce_order'] as $key => $val) {

        $val = commerce_order_load($val->order_id);

        foreach ($val->commerce_line_items['und'] as $line) {
          $line_item = commerce_line_item_load($line['line_item_id']);
          $product_id = $line_item->commerce_product['und'][0]['product_id'];
        }
        $product_item = commerce_product_load($product_id);

        if ($product_item->type == "product" && $product_id == $license->product_id) {
          $previous_order = $val;
          break;
        }
      }

      $date_next_invoice = strtotime("+1 month", $previous_order->created);

      if ($date_next_invoice < $timestamp_now) {
        $generated_new_order = _usine_membership_generate_recurring_order($previous_order, $license);

        $params = array('subject'     => variable_get_value('usine_email_monthly_payment_notification_user_subject'),
                        'body'        => variable_get_value('usine_email_monthly_payment_notification_user_body'));
        drupal_mail('usine_membership', 'usine_membership_monthly_payment_notice', $license_user->mail, language_default(), $params);
      }

      /*
       * Deleting license when termination date has passed
       */
      if (!is_null($license_user->field_user_membership_term_date['und'][0]['value']) && strtotime($license_user->field_user_membership_term_date['und'][0]['value']) < $timestamp_now){
        $license->status = 3;
        entity_save("commerce_license", $license);
        //entity_delete("commerce_license", $license->id);
        $params = array('subject'     => variable_get_value('usine_email_termination_confirmation_subject'),
                        'body'        => variable_get_value('usine_email_termination_confirmation_body'));

        drupal_mail('usine_membership', 'usine_membership_termination_confirmation', $license_user->mail, language_default(), $params);
      }
    }
  }
}


function usine_membership_cronapi($op, $job = NULL) {
  $items['usine_membership_expire_notification'] = array(
    'description' => 'Send expiritation notices',
    'rule' => '0 1 * * *' // Everyday at 1 AM
  );

  return $items;
}


/*
 * Expiration notifications 1 week before end of subscription
 */
function usine_membership_expire_notification() {
  $query  = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'commerce_license');
  $result = $query->execute();

  foreach ($result['commerce_license'] as $key => $val) {
    $license                = entity_load_single('commerce_license', $val->license_id);
    $license_user           = user_load($license->uid);

    if($license->status == 2) {

      $license_product          = commerce_product_load($license->product_id);
      $timestamp_expire         = strtotime(date('Y-m-d', $license->granted) . ' +' . $license_product->field_usine_membership_duration['und'][0]['value'] . ' months');
      $timestamp_expire_notice  = strtotime(date('Y-m-d', $timestamp_expire) . ' -1week');
      $timestamp_now          = time();

      if (date("Y-m-d") == date("Y-m-d", $timestamp_expire_notice) && $license_user->field_user_membership_term['und'][0]['value'] != 1 && is_null($license_user->field_user_membership_term_date['und'][0]['value']) ){
        $params = array('subject'     => variable_get_value('usine_email_expire_notice_subject'),
                        'body'        => variable_get_value('usine_email_expire_notice_body'));
        drupal_mail('usine_membership', 'usine_membership_expiration_user', $license_user->mail, language_default(), $params);
        drupal_mail('usine_membership', 'usine_membership_expiration_user', "tseropian@gmail.com",  language_default(), $params);
      }
    }
  }
}


?>
