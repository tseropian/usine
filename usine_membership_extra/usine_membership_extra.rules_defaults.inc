<?php
/**
 * @file
 * usine_membership_extra.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function usine_membership_extra_default_rules_configuration() {
  $items = array();
  $items['rules_usine_membership_send_termination_date_notification'] = entity_import('rules_config', '{ "rules_usine_membership_send_termination_date_notification" : {
      "LABEL" : "Usine - Membership - Send Termination Date Notification",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "usine.io" ],
      "REQUIRES" : [ "rules", "variable_email" ],
      "ON" : { "user_update" : [] },
      "IF" : [
        { "user_has_role" : { "account" : [ "account" ], "roles" : { "value" : { "8" : "8" } } } },
        { "entity_has_field" : { "entity" : [ "account" ], "field" : "field_user_membership_term_date" } },
        { "NOT data_is_empty" : { "data" : [ "account:field-user-membership-term-date" ] } }
      ],
      "DO" : [
        { "variable_email_mail" : {
            "to" : [ "account:mail" ],
            "variable" : "usine_email_expiration_confirmation_[mail_part]",
            "language" : "default"
          }
        },
        { "mail" : {
            "to" : "hello@usine.io",
            "subject" : "R\\u00e9siliation de compte : [account:mail]",
            "message" : "- Membre : [account:mail] \\u003Cbr \\/\\u003E\\r\\n- Date d\\u0027expiration : [account:field-user-membership-term-date]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}
