<?php
function usine_paymill_cronapi($op, $job = NULL) {

  $items['usine_paymill_expiring_cards'] = array(
    'description' => 'Send user notice for expiring cards',
    'rule' => '0 0 1 * *', // Every Month
  );

  return $items;
}

function usine_paymill_expiring_cards() {
	$query = new EntityFieldQuery();
	$query->entityCondition('entity_type', 'commerce_cardonfile')->propertyCondition('card_exp_year', date("Y"))->propertyCondition('card_exp_month', date("m"));
	$result = $query->execute();
	foreach ($result["commerce_cardonfile"] as $key => $val) {
        	$card           = entity_load_single("commerce_cardonfile", array($key));
	        $card_user      = user_load($card->uid);

		$params = array('subject'     => variable_get_value('usine_email_expiration_card_user_subject'),
				'body'        => variable_get_value('usine_email_expiration_card_user_body'));
		drupal_mail('usine_paymill', 'usine_paymill_expiration_card_user', $card_user->mail, language_default(), $params);

	}

}
?>
