<?php
/**
 * @file
 * usine_register.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function usine_register_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ontheweb|profile2|usine_profile|form';
  $field_group->group_name = 'group_ontheweb';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'usine_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'On The Web',
    'weight' => '5',
    'children' => array(
      0 => 'field_user_website',
      1 => 'field_user_facebook_profile',
      2 => 'field_user_github_profile',
      3 => 'field_user_linkedin_profile',
      4 => 'field_user_twitter_profile',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'On The Web',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-ontheweb field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_ontheweb|profile2|usine_profile|form'] = $field_group;

  return $export;
}
