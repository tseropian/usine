<?php
/**
 * @file
 * usine_register.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function usine_register_default_rules_configuration() {
  $items = array();
  $items['rules_usine_create_startup_profile'] = entity_import('rules_config', '{ "rules_usine_create_startup_profile" : {
      "LABEL" : "Usine - Startup - Create profile",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "usine.io" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "user_insert" : [] },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "account" ], "field" : "field_user_profile" } },
        { "data_is" : {
            "data" : [ "account:field-user-profile" ],
            "op" : "IN",
            "value" : { "value" : { "39" : "39" } }
          }
        }
      ],
      "DO" : [
        { "user_add_role" : { "account" : [ "account" ], "roles" : { "value" : { "5" : "5" } } } },
        { "drupal_message" : { "message" : "Bienvenue chez Usine IO. Vous pouvez d\\u00e8s \\u00e0 pr\\u00e9sent \\u00e9diter le profil de votre entreprise." } },
        { "redirect" : { "url" : "node\\/add\\/startup?edit[title_field][und][0][value]=[account:field-user-startup-name]\\u0026edit[field_startup_staff_nb][und]=[account:field-startup-staff-nb]" } }
      ]
    }
  }');
  $items['rules_usine_redirect_individuals_to_usine_profile'] = entity_import('rules_config', '{ "rules_usine_redirect_individuals_to_usine_profile" : {
      "LABEL" : "Usine - Individuals - Redirect To Memberships ",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "usine.io" ],
      "REQUIRES" : [ "rules", "php" ],
      "ON" : { "user_insert" : [] },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "account" ], "field" : "field_user_profile" } },
        { "php_eval" : { "code" : "if($account-\\u003Efield_user_profile[\\u0027und\\u0027][0][\\u0027tid\\u0027] == 38){return true;}" } }
      ],
      "DO" : [
        { "user_add_role" : { "account" : [ "account" ], "roles" : { "value" : { "4" : "4" } } } },
        { "redirect" : { "url" : "memberships\\/individual" } }
      ]
    }
  }');
  $items['rules_usine_redirect_startup_to_invite_people'] = entity_import('rules_config', '{ "rules_usine_redirect_startup_to_invite_people" : {
      "LABEL" : "Usine - Startup - Redirect to Invite People",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "usine.io" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert--startup" : { "bundle" : "startup" } },
      "IF" : [
        { "user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "5" : "5" } }
          }
        }
      ],
      "DO" : [
        { "drupal_message" : { "message" : "Votre processus d\\u0027inscription est presque termin\\u00e9. Il est temps de choisir un abonnement adapt\\u00e9 \\u00e0 vos besoins et d\\u0027inviter ensuite les membres de votre \\u00e9quipe si indiqu\\u00e9." } },
        { "redirect" : { "url" : "memberships\\/startup" } }
      ]
    }
  }');
  $items['rules_usine_startup_redirect_after_user_profile_update'] = entity_import('rules_config', '{ "rules_usine_startup_redirect_after_user_profile_update" : {
      "LABEL" : "Usine - Startup - Redirect After User Profile Update",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "OWNER" : "rules",
      "TAGS" : [ "usine.io" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "user_update" : [] },
      "IF" : [
        { "user_has_role" : { "account" : [ "account" ], "roles" : { "value" : { "5" : "5" } } } }
      ],
      "DO" : [ { "redirect" : { "url" : "my-startup" } } ]
    }
  }');
  $items['usine_register_usine_corporate_create_profile'] = entity_import('rules_config', '{ "usine_register_usine_corporate_create_profile" : {
      "LABEL" : "Usine - Corporate - Create profile",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "usine.io" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "user_insert" : [] },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "account" ], "field" : "field_user_profile" } },
        { "data_is" : {
            "data" : [ "account:field-user-profile" ],
            "op" : "IN",
            "value" : { "value" : { "57" : "57" } }
          }
        }
      ],
      "DO" : [
        { "user_add_role" : { "account" : [ "account" ], "roles" : { "value" : { "12" : "12" } } } },
        { "drupal_message" : { "message" : "Bienvenue chez Usine IO. Vous pouvez d\\u00e8s \\u00e0 pr\\u00e9sent \\u00e9diter le profil de votre entreprise." } },
        { "redirect" : { "url" : "node\\/add\\/startup?edit[title_field][und][0][value]=[account:field-user-startup-name]\\u0026edit[field_startup_staff_nb][und]=[account:field-startup-staff-nb]" } }
      ]
    }
  }');
  $items['usine_register_usine_corporate_memberships'] = entity_import('rules_config', '{ "usine_register_usine_corporate_memberships" : {
      "LABEL" : "Usine - Corporate - Redirect to Memberships",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "usine.io" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert--startup" : { "bundle" : "startup" } },
      "IF" : [
        { "user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "12" : "12" } }
          }
        }
      ],
      "DO" : [
        { "drupal_message" : { "message" : "Votre processus d\\u0027inscription est presque termin\\u00e9. Il est temps de choisir un abonnement adapt\\u00e9 \\u00e0 vos besoins et d\\u0027inviter ensuite les membres de votre \\u00e9quipe si indiqu\\u00e9." } },
        { "redirect" : { "url" : "memberships\\/corporate" } }
      ]
    }
  }');
  $items['usine_register_usine_corporate_redirect_to_memberships'] = entity_import('rules_config', '{ "usine_register_usine_corporate_redirect_to_memberships" : {
      "LABEL" : "Usine - Corporate - Redirect To Memberships",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "OWNER" : "rules",
      "TAGS" : [ "usine.io" ],
      "REQUIRES" : [ "rules", "php" ],
      "ON" : { "user_insert" : [] },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "account" ], "field" : "field_user_profile" } },
        { "php_eval" : { "code" : "if($account-\\u003Efield_user_profile[\\u0027und\\u0027][0][\\u0027tid\\u0027] == 57) {return true;}" } }
      ],
      "DO" : [
        { "user_add_role" : { "account" : [ "account" ], "roles" : { "value" : { "12" : "12" } } } },
        { "redirect" : { "url" : "memberships\\/corporate" } }
      ]
    }
  }');
  return $items;
}
