<?php
/**
 * @file
 * usine_register.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function usine_register_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_user__user';
  $strongarm->value = array(
    'view_modes' => array(
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'account' => array(
          'weight' => '2',
        ),
        'timezone' => array(
          'weight' => '3',
        ),
        'picture' => array(
          'weight' => '9',
        ),
        'wysiwyg' => array(
          'weight' => '8',
        ),
        'masquerade' => array(
          'weight' => '13',
        ),
        'contact' => array(
          'weight' => '12',
        ),
        'locale' => array(
          'weight' => '10',
        ),
        'l10nclient' => array(
          'weight' => '11',
        ),
        'password_policy' => array(
          'weight' => '0',
        ),
      ),
      'display' => array(
        'masquerade' => array(
          'default' => array(
            'weight' => '7',
            'visible' => TRUE,
          ),
        ),
        'profile_usine_profile' => array(
          'default' => array(
            'weight' => '4',
            'visible' => TRUE,
          ),
        ),
        'summary' => array(
          'default' => array(
            'weight' => '15',
            'visible' => FALSE,
          ),
        ),
        'usine_my_startup_entity_view_1' => array(
          'default' => array(
            'weight' => '10',
            'visible' => TRUE,
          ),
        ),
        'usine_user_membership_entity_view_1' => array(
          'default' => array(
            'weight' => '8',
            'visible' => TRUE,
          ),
        ),
        'usine_resource_booking_calendar_entity_view_1' => array(
          'default' => array(
            'weight' => '7',
            'visible' => TRUE,
          ),
        ),
        'usine_individual_booking_dashboard_entity_view_1' => array(
          'default' => array(
            'weight' => '15',
            'visible' => TRUE,
          ),
        ),
        'usine_individual_booking_dashboard_entity_view_1_form' => array(
          'default' => array(
            'weight' => '12',
            'visible' => FALSE,
          ),
        ),
        'usine_membership_profile_entity_view_1' => array(
          'default' => array(
            'weight' => '10',
            'visible' => TRUE,
          ),
        ),
        'usine_membership_profile_entity_view_2' => array(
          'default' => array(
            'weight' => '10',
            'visible' => TRUE,
          ),
        ),
        'usine_user_termination_entity_view_1' => array(
          'default' => array(
            'weight' => '15',
            'visible' => TRUE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_user__user'] = $strongarm;

  return $export;
}
